package com.kim.limit.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.limit.server
 * @FileName: LimitApplication.java
 * @Description: The LimitApplication is...
 * @Author: kimwu
 * @Time: 2020-12-25 10:38:03
 */
@SpringBootApplication
public class LimitApplication {

    public static void main(String[] args) {
        SpringApplication.run(LimitApplication.class, args);
    }

}

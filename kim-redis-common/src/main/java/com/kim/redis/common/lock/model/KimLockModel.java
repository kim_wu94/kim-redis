package com.kim.redis.common.lock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.redis.common.lock.model
 * @FileName: KimLockModel.java
 * @Description: The KimLockModel is...
 * @Author: kimwu
 * @Time: 2020-12-19 14:13:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KimLockModel {

    /**
     * 锁的名称
     */
    private String name;

    /**
     * 锁的value
     */
    private String value;

    /**
     * 过期时间(如果不设置默认60s)
     */
    private Integer expireTime;

}

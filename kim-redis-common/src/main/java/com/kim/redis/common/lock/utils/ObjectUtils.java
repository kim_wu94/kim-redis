package com.kim.redis.common.lock.utils;

import com.kim.redis.common.lock.model.KimLockModel;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.redis.common.lock.utils
 * @FileName: ObjectUtils.java
 * @Description: The ObjectUtils is...
 * @Author: kimwu
 * @Time: 2020-12-19 14:23:40
 */
@Slf4j
public class ObjectUtils {


    /**
     * 判断该对象是否: 返回ture表示所有属性为null  返回false表示不是所有属性都是null
     * @param obj
     * @return
     * @throws Exception
     */
    public static boolean isAllFieldNull(Object obj) throws Exception {
        // 得到类对象
        Class stuCla = (Class) obj.getClass();
        //得到属性集合
        Field[] fs = stuCla.getDeclaredFields();
        boolean flag = true;
        //遍历属性
        for (Field f : fs) {
            // 设置属性是可以访问的(私有的也可以)
            f.setAccessible(true);
            // 得到此属性的值
            Object val = f.get(obj);
            //只要有1个属性不为空,那么就不是所有的属性值都为空
            if (val != null) {
                flag = false;
                break;
            }
        }
        return flag;
    }


}


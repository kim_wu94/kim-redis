package com.kim.redis.common.lock.utils;

import com.kim.redis.common.lock.lock.RedisDistributionLockPlus;
import org.aspectj.lang.JoinPoint;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.redis.common.lock.utils
 * @FileName: CurentMapUtils.java
 * @Description: The CurentMapUtils is...
 * @Author: kimwu
 * @Time: 2020-12-19 14:31:37
 */
public class CurentMapUtils {


    public static final Map<String, RedisDistributionLockPlus> currentThreadLock = new ConcurrentHashMap<>();

    /**
     * 获取当前锁在map中的key
     * @param joinPoint
     * @param klock
     * @return
     */
    public static String getCurrentLockId(){
        Long curentLock= Thread.currentThread().getId();
        return curentLock.toString();
    }
}

package com.kim.redis.cache.service;

import com.kim.redis.cache.entity.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账户信息表 服务类
 * </p>
 *
 * @author KimWu
 * @since 2020-12-25
 */
public interface IUserAccountService extends IService<UserAccount> {

}

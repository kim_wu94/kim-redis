package com.kim.redis.cache.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kim.redis.cache.entity.UserAccount;
import com.kim.redis.common.cache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * <p>
 * 用户账户信息表 Mapper 接口
 * </p>
 *
 * @author KimWu
 * @since 2020-12-25
 */
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}

package com.kim.lock.server.demo;

import com.kim.redis.common.lock.annotation.KimLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.lock.server.demo
 * @FileName: demo.java
 * @Description: The demo is...
 * @Author: kimwu
 * @Time: 2020-12-18 14:52:09
 */
@RestController
@RequestMapping("/v1/redisbody")
@Slf4j
public class DemoController {

    @PostMapping("/test")
    @KimLock(name = "redis.lock", expireTime = 10)
    public void test() {
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("{} --> 结束操作");
    }

}
